import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import { Router } from '@angular/router';
import { getAttrsForDirectiveMatching } from '@angular/compiler/src/render3/view/util';



@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private AFauth: AngularFireAuth, private route:Router) { }

  login(email: string, password:string){

    return new Promise((resolve, rejected) => {
      this.AFauth.signInWithEmailAndPassword(email, password).then(user => {
        resolve(user)
        //console.log('Token',this.AFauth.getRedirectResult());
      }).catch( err => rejected(err));
    })
  }

  logout(){
    this.AFauth.signOut().then( () => {
      this.route.navigate(['/home']);
    })
  }

  setUserId(){
    this.AFauth.onAuthStateChanged(user => {localStorage.setItem('User', JSON.stringify(user));})
  }

  getUserId(){
    return JSON.parse( localStorage.getItem('User'));
  }

  getUserLocation(){
    return JSON.parse( localStorage.getItem('UserLocation'));
  }

  



}
