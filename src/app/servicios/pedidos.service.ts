import { PedidosInterface } from './../interfaces/pedidos.interface';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class PedidosService {

  tiendas: Observable<PedidosInterface[]>;
  pedidos: any[] = [];
  tienda: string = JSON.parse( localStorage.getItem('tienda'));
  tiendaPedidos: any[] = [];

  // Prueba
  recover = JSON.parse( localStorage.getItem(`datosTienda${this.tienda.replace(/ /g, "")}`));
  listPedidos: any[] = [];
  userData = [];

  constructor(
    public localizacion: Geolocation,
    public afd: AngularFireDatabase) { }


  getTienda(){
    return this.tiendas = JSON.parse( localStorage.getItem(`datosTienda${this.tienda.replace(/ /g, "")}`));
  }

  getPedidos(){
    return this.pedidos =  JSON.parse( localStorage.getItem('cantidadTotal'));
  }

  getListPedidos(){
    this.listPedidos =  JSON.parse( localStorage.getItem('datosPedido'));
    console.log('Cant', this.listPedidos.length);
    return this.listPedidos;
  }

  deleteStorage(){
    localStorage.removeItem('cantidadTotal');
    localStorage.removeItem('cantidadTotal');
  }

  positionUser(){
     return this.localizacion.getCurrentPosition().then((resp) => {
      console.log('Posicion', resp)
      const lat = resp.coords.latitude;
      const long = resp.coords.longitude;
      localStorage.setItem('PositionUser', JSON.stringify({lat,long}))
     }).catch((error) => {
       console.log('Error getting location', error);
     });

  }

  saveProducts(){}

  addPedido(pedido){
    this.afd.list('pedidos/').push(pedido);
  }

  getUserData(userId: string){
    return this.afd.list('/usuarios/'+userId).snapshotChanges();
  }




}


