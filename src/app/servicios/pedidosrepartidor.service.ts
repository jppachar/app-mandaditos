import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PedidosRepartidorService {
  // pedidos: AngularFireList<any>;
  constructor(private db: AngularFireDatabase) { 
    
  }

  getAll(){
    return this.db.list('/pedidos').snapshotChanges();
  }

  findOneById(id: string){
    return this.db.object('/pedidos/'+id).snapshotChanges();
  }

  accept(id: string, idRepartidor: string): Promise<any>{
    let reference = this.db.database.ref('/pedidos/'+id);
    return reference.update({ estado: 'EN PROCESO', repartidor: 'ezaAafarX2huclLZst9U0z411862'})
  }

}
