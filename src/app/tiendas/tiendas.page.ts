import { Component, OnInit, OnDestroy, ViewChild, TemplateRef } from '@angular/core';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { NavController, NumericValueAccessor } from '@ionic/angular';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';

import { LoadingController } from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Observable, Subscription } from 'rxjs';
import { title } from 'process';
import { Router, ActivatedRoute } from '@angular/router';


import { AuthService } from '../servicios/auth.service';
import { PedidosService } from '../servicios/pedidos.service';



declare var google;
// declare var conver;



@Component({
  selector: 'app-tiendas',
  templateUrl: './tiendas.page.html',
  styleUrls: ['./tiendas.page.scss'],
})


export class TiendasPage implements OnInit, OnDestroy {

  // @ViewChild('map', { static: true }) mapElement;
  userLocationMarker: any;
  userCoordinates: Geoposition;
  tiendaSub: Subscription;
  mapElem: HTMLElement;
  map: any;
  stores: any[];
  infoTienda={
    nombre: " ",
    direccion: " "
  };
  //infowindow: any;


  constructor(
    public geolocation: Geolocation, 
    public loadingCtrl: LoadingController, 
    public router: Router,  public route: ActivatedRoute, 
    public authservice: AuthService){}


  ngOnInit() {
    this.mapElem = document.getElementById('map');
    this.initMap();
  }

  async initMap() {
    //-------------------------------------------------//
    var geocoder = new google.maps.Geocoder(); // Primer paso crear una variable geocoder => GEOCODER para transformar una ubicación lat-long en formato legible (dirección)
    //------------------------------------------------//

    const geo = await this.geolocation.getCurrentPosition();
    const center = new google.maps.LatLng(geo.coords.latitude, geo.coords.longitude);
    const mapOptions = {
      center,
      zoom: 18,
      zoomControl: false,
      streetViewControl: false,
      disableDoubleClickZoom: true,
      fullscreenControl: false,
      mapTypeControl: false,
    };

    var dirFisica = '';

    geocoder.geocode({ location: center }, function(results,status) {//
      if(status === 'OK')  {
        console.log('RECUPERACIÓN DEL RESULTADO' + results[0].formatted_address);

        dirFisica = results[0].formatted_address;
        console.log(' DIRECCIÓN ACTUAL:' + dirFisica);
        localStorage.setItem('UserLocation', JSON.stringify(dirFisica));
      }else{
        console.log('Error');
      }
      
    });

    //--------------------------------------------------------//
    

    this.map = new google.maps.Map(this.mapElem, mapOptions);
    this.userLocationMarker = new google.maps.Marker({
      title:dirFisica,
      map: this.map,
      position: center,
      zoom: 18
    });

    const placesService = new google.maps.places.PlacesService(this.map);
    const placesServiceConfig = {
      location: {
        lat: geo.coords.latitude,
        lng: geo.coords.longitude,
      }, radius: 1000, type: ['supermarket']
    };
    placesService.nearbySearch(placesServiceConfig, (results: any, status, pagination) => {
      if (status !== 'OK') {
        return;
      }
      this.createMarkers(results);
    });

    this.tiendaSub = this.geolocation.watchPosition().subscribe(res => {
      this.userCoordinates = res;
    });
    //this.infowindow = new google.maps.InfoWindow();
  }

  



  createMarkers(places) {
    const bounds = new google.maps.LatLngBounds();
    const placesList = document.getElementById('places');
    const image = {
      url: 'assets/icon/pack.png',
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(25, 25)
    };

    places.forEach(place => {
      const marker = new google.maps.Marker({
        map: this.map,
        icon: image,
        title: place.name,
        position: place.geometry.location
      });
      const li: HTMLElement = document.createElement('li');
      li.textContent = place.name;
      placesList.appendChild(li);
      bounds.extend(place.geometry.location);

      var marketPedido = '<ion-button (click)="onSubmitMarketPedido()" fill="solid" expand="" color="danger">Pedir</ion-button>';

      var infowindow = new google.maps.InfoWindow({
        content: marketPedido
      });

      google.maps.event.addListener(marker, 'click', function() {
        //this.infowindow.open(Map,marker);
        console.log("Click me!");
        var lugar = place.name;
        console.log(place);
        this.verif=true;
        //this.
        //console.log(this.verif);

        // this.router.navigate(['/add-pedido', place.name]);
        //infowindow.open(this.map, marker);
        // this.router.navigate(['/add-pedido', place.name], {relativeTo: this.route});
        this.infoTienda={
          nombre: place.name,
          direccion: place.vicinity
        };
        localStorage.setItem(`datosTienda${place.name.replace(/ /g, "")}`, JSON.stringify(this.infoTienda));
        location.assign(`http://localhost:8100/add-pedido/${place.name}`);
      });
    });
    this.map.fitBounds(bounds);
  }
  


  foo() {
    console.log(this.userCoordinates);
  }

  ngOnDestroy(): void {
    this.tiendaSub.unsubscribe();
  }

  onSubmitMarketPedido()
  {
    this.router.navigate(['/add-pedido']);
  }

  onLogout(){
    this.authservice.logout();
    localStorage.clear();
  }

  // getGeolocation() {
  //   this.geolocation.getCurrentPosition().then((geoposition) => {
  //     this.lat = geoposition.coords.latitude;
  //     this.lon = geoposition.coords.longitude;
  //   });

  //   let watch = this.geolocation.watchPosition();
  //   watch.subscribe((data) => {
  //     this.lat2 = data.coords.latitude;
  //     this.lon2 = data.coords.longitude;
  //   });
  // }
  // initMap() {

  // }

  // ngOnInit() {
  //   this.loadMap();
  // }

  // async loadMap() {


  //   const loading = await this.loadingCtrl.create();
  //   loading.present();
  //   loading.dismiss();
  //   const rta = await this.geolocation.getCurrentPosition();
  //   const myLatLng = {
  //     lat: rta.coords.latitude,
  //     lng: rta.coords.longitude
  //   };


  //   const mapEle: HTMLElement = document.getElementById('map');



  //   const map = new google.maps.Map(mapEle, {
  //     center: myLatLng,
  //     zoom: 14

  //   });


  //   //-----------------------
  //   var geocoder = new google.maps.Geocoder;

  //   geocodeLatLng(geocoder, myLatLng);
  //   // Transformar de Lat y Lon en calles.



  //   function geocodeLatLng(geocoder, mylatlon) {





  //     geocoder.geocode({ 'location': mylatlon }, function (results, status) {
  //       if (status === 'OK') {
  //         if (results[0]) {

  //           var placesList = document.getElementById('sitioactual');
  //           console.log(results[0].formatted_address);
  //           var li = document.createElement('p');
  //           li.textContent = "Ubicación actual: " + results[0].formatted_address;
  //           placesList.appendChild(li);
  //           console.log("li", li);
  //         }
  //       }

  //     });
  //   }




  //   //-----------------------

  //   // Create the places service.





  //-----------------------

  // function createMarkers(places) {
  //   var bounds = new google.maps.LatLngBounds();
  //   var placesList = document.getElementById('places');

  //   for (var i = 0, place; place = places[i]; i++) {
  //     var image = {
  //       url: 'assets/icon/pack.png',
  //       size: new google.maps.Size(71, 71),
  //       origin: new google.maps.Point(0, 0),
  //       anchor: new google.maps.Point(17, 34),
  //       scaledSize: new google.maps.Size(25, 25)
  //     };

  //     var marker = new google.maps.Marker({
  //       map: map,
  //       icon: image,
  //       title: place.name,
  //       position: place.geometry.location
  //     });

  //     var li = document.createElement('li');
  //     li.textContent = place.name;
  //     placesList.appendChild(li);

  //     bounds.extend(place.geometry.location);
  //   }
  //   map.fitBounds(bounds);
  // }





  //   // Marker para las posiciones

  //   google.maps.event.addListenerOnce(map, 'idle', () => {
  //     console.log('added');
  //     loading.dismiss();

  //     const usuario = new google.maps.Marker({
  //       position: {
  //         lat: myLatLng.lat,
  //         lng: myLatLng.lng
  //       },
  //       zoom: 8,
  //       map: map,
  //       title: 'Usuario'
  //     });
  /*
  

  const marker = new google.maps.Marker({
    position: {
      lat: -3.999887,
      lng:-79.203832,
      
    },
    zoom: 8,
    map: map,
    title: 'Zerimar'
  });
  
  marker.setIcon('assets/icon/pack.png');


  const marker2 = new google.maps.Marker({
    position: {
      lat: -3.988162, 
      lng: -79.203022
    },
    zoom: 8,
    map: map,
    title: 'Gran AKÍ Loja'
  });
  marker2.setIcon('assets/icon/pack.png');
  const marker3 = new google.maps.Marker({
    position: {
      lat: -4.012107,
      lng: -79.202602
    },
    zoom: 8,
    map: map,
    title: 'Supermaxi'
  });
  marker3.setIcon('assets/icon/pack.png');
  const marker4 = new google.maps.Marker({
    position: {
      lat: -3.998873, 
      lng: -79.203912
    },
    zoom: 8,
    map: map,
    title: 'Economax'
  });
  marker4.setIcon('assets/icon/pack.png');
  const marker5 = new google.maps.Marker({
    position: {
      lat: -3.999531, 
      lng: -79.205519 
    },
    zoom: 8,
    map: map,
    title: 'Mercamax'
  });
  marker5.setIcon('assets/icon/pack.png');
  const marker6 = new google.maps.Marker({
    position: {
      lat: -4.021961, 
      lng: -79.217322
    },
    zoom: 8,
    map: map,
    title: 'Micromercado Emili'
  });
  marker6.setIcon('assets/icon/pack.png');
  */

  // })


  // }

}
