import { Role } from '../enums/role.enum';

export interface User {
    id: string;
    nombres: string;
    apellidos: string;
    correo: string;
    contrasena?: string;
    createdAt: Date;
    telefono: string;
    rol: Role;
}
