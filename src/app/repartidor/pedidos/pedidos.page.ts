import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/interfaces/user';
import { Role } from 'src/app/enums/role.enum';
import { Observable } from 'rxjs';
import { Platform } from '@ionic/angular';
import { PedidosRepartidorService } from 'src/app/servicios/pedidosrepartidor.service';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.page.html',
  styleUrls: ['./pedidos.page.scss'],
})
export class PedidosPage implements OnInit {
  // private items = [];
  items: Observable<any[]>;
  item: any = [];

  // // selected: any;

  private currentUser: User = {
    id: 'ezaAafarX2huclLZst9U0z411862',
    nombres: 'John',
    apellidos: 'Doe',
    correo: 'johndoe@email.com',
    rol: Role.REPARTIDOR,
    createdAt: new Date(),
    telefono: '0979615531'
  };

  // private items: any = [
  //   {
  //     id: '1',
  //     client: 'Juan Pablo Pachar',
  //     ubicacion: 'Cdla. Zamora la Pileta',
  //     telefono: '0991883896',
  //     cantidadArticulos: 12
  //   },
  //   {
  //     id: '2',
  //     client: 'Tania Granda',
  //     ubicacion: 'Cdla. Zamora la Pileta',
  //     telefono: '0991883896',
  //     cantidadArticulos: 12
  //   },
  //   {
  //     id: '3',
  //     client: 'Ricardo Arrobo',
  //     ubicacion: 'Cdla. Zamora la Pileta',
  //     telefono: '0991883896',
  //     cantidadArticulos: 12
  //   },
  //   {
  //     id: '4',
  //     client: 'Juan Pablo Pachar',
  //     ubicacion: 'Cdla. Zamora la Pileta',
  //     telefono: '0991883896',
  //     cantidadArticulos: 12
  //   },
  //   {
  //     id: '5',
  //     client: 'Tania Granda',
  //     ubicacion: 'Cdla. Zamora la Pileta',
  //     telefono: '0991883896',
  //     cantidadArticulos: 12
  //   },
  //   {
  //     id: '6',
  //     client: 'Ricardo Arrobo',
  //     ubicacion: 'Cdla. Zamora la Pileta',
  //     telefono: '0991883896',
  //     cantidadArticulos: 12
  //   }
  // ]




  constructor(private router: Router, private pedidosRepartidorService: PedidosRepartidorService, private platform: Platform) {
  }
  ngOnDestroy(): void {
    this.items
  }

  ngOnInit() {
    this.items = this.pedidosRepartidorService.getAll().pipe(
      map(data => {
        // console.log(data);
        let temp = [];
        data.forEach(val =>{
          // console.log(val);
          this.item = val.payload.val();
          // console.log(this.item);
          let quantity = 0;
          Array.from(this.item.detalle).forEach((d: any) => {
            // console.log(d.listado);
            d.listado.forEach(i => {
              quantity += i.cantidad;
            })
          })
          /* this.item.detalle.forEach(d => {
            d.listado.forEach(i => {
              quantity += i.cantidad;
            });
          }); */
          this.item['cantidad'] = quantity;
          this.item['id'] = val.key;
          temp.push(this.item);
        });
        return temp;
      })
    );
  }

  

  navigateToDetail(pedido) {
    this.router.navigate(['detalle-pedido', pedido.id], {skipLocationChange: true} );
  }
}
