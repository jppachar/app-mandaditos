import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { PedidosRepartidorService } from 'src/app/servicios/pedidosrepartidor.service';
import { Observable, pipe, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-detalle-pedido',
  templateUrl: './detalle-pedido.page.html',
  styleUrls: ['./detalle-pedido.page.scss'],
})
export class DetallePedidoPage implements OnInit, OnDestroy {
  // pedido: Observable<any>;
  pedido: any;
  suscripcion: Subscription;

  private items: any = [
    {
      local: "Supermaxi",
      direccion: "La pradera",
      list: [
        {
          cantidad: 2,
          descripcion: "Coca 3 lts"
        },
        {
          cantidad: 12,
          descripcion: "Huevos"
        },
        {
          cantidad: 2,
          descripcion: "Lechuga crespa"
        }
      ]
    },
    {
      local: "Farmacias Cuxibamba",
      direccion: "18 de Noviembre",
      list: [
        {
          cantidad: 100,
          descripcion: "Aspirinas"
        },
        {
          cantidad: 12,
          descripcion: "Mascarillas"
        }
      ]
    },
    {
      local: "Grafica Santiago",
      direccion: "18 de Noviembre",
      list: [
        {
          cantidad: 100,
          descripcion: "Hojas de papel bond"
        },
        {
          cantidad: 1,
          descripcion: "Caja de lapices"
        },
        {
          cantidad: 3,
          descripcion: "Marcadores negros"
        }
      ]
    }
  ]

  constructor(private activatedRoute: ActivatedRoute, private pedidosRepartidorService: PedidosRepartidorService, private router: Router, private alertCtrl: AlertController) { }
  ngOnDestroy(): void {
    this.suscripcion.unsubscribe();
  }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get("id");
    if (id) {
      this.suscripcion = this.pedidosRepartidorService.findOneById(id)
        .subscribe(p => {
          this.pedido = p.payload.val();
          this.pedido.id = p.key;
        });
    }
  }

  async backButton() {
    this.router.navigate(['pedidos-solicitados']);
  }

  async accept(pedido) {
    console.log('pedido')
    const alert = await this.alertCtrl.create({
      header: 'Estas seguro de aceptar esta solicitud?',
      message: 'Una vez aceptada la solicitud tendrás la responsabilidad de atender el pedido',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Aceptar pedido',
          handler: async () => {
            this.pedidosRepartidorService.accept(pedido.id, 'id_repartidor');
            let confirmation = await this.alertCtrl.create({
              subHeader: 'Pedido confirmado'
            });
            confirmation.present();
            setTimeout(() => {
              confirmation.dismiss();
              this.router.navigate(['pedidos-solicitados']);
            }, 1000);
          }
        }
      ]
    });
    alert.present();
  };

}
