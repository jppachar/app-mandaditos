import { AuthService } from './../servicios/auth.service';
import { PedidosInterface } from './../interfaces/pedidos.interface';
import { Component, OnInit } from '@angular/core';
import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed
} from '@capacitor/core';

const { PushNotifications, Modals } = Plugins;

import { PedidosService } from '../servicios/pedidos.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.page.html',
  styleUrls: ['./pedidos.page.scss'],
})
export class PedidosPage implements OnInit {

  tiendas: PedidosInterface[] = [];
  pedidos: any[] = [];
  prueba =  new Array();
  user: any;
  id: string;

  ocultarBtn: boolean = true;
  estado = "Solicitar";
  // Objeto del pedido
  addpedido =
    {
      cliente: {direccion: '', id: '', nombre: '', telefono: ''},
      detalle: {  direccion: '', tienda: '', listado: {} },
      estado: 'CREADO',
      repartidor: 'SIN ASIGNAR'
    };

  constructor(
      private listPedidos: PedidosService,
      public router: Router,
      private alertCtrl: AlertController,
      private location: Location,
      private authC: AuthService) { }

  ngOnInit() {
    this.id = this.authC.getUserId().uid;
    this.getTiendas();
    this.listPedidos.positionUser();
    this.listPedidos.getListPedidos();
    this.savePedido(this.id);

    PushNotifications.register();

    PushNotifications.addListener('registration', (token: PushNotificationToken) => {
      alert('Push registration success, token: ' + token.value);
      console.log('Push registration success, token: ' + token.value);
    });

    PushNotifications.addListener('registrationError', (error: any) => {
      alert('Error on registration: ' + JSON.stringify(error));
    });

    PushNotifications.addListener('pushNotificationReceived', (notification: PushNotification) => {
      const audio1 = new Audio('assets/audio.mp3');
      console.log('Audio');
      audio1.play();
      // alert('Push received: ' + JSON.stringify(notification));
      console.log('Push received: ', notification);

      const alertRet = Modals.alert({
        title: notification.title,
        message: notification.body
      });
    });

    PushNotifications.addListener('pushNotificationActionPerformed', (notification: PushNotificationActionPerformed) => {
      alert('Push action performed: ' + JSON.stringify(notification));
      console.log('Push action performed: ' + notification);
    });
  }

  getTiendas(){
      this.getPedidos();
      this.tiendas = this.listPedidos.getTienda();
      if(this.tiendas != null){
        console.log('TiendasInterface: ', this.tiendas);
      }else{
        this.tiendas = [];
      }
      this.addpedido.detalle.tienda = this.listPedidos.getTienda().nombre;
      this.addpedido.detalle.direccion = this.listPedidos.getTienda().direccion;
    }

  getPedidos(){
      this.pedidos = this.listPedidos.getPedidos();
      if (this.pedidos != null){
        console.log('Pedidos',this.pedidos);
      }else{
        this.pedidos  = [0];
      }
      this.addpedido.detalle.listado = this.listPedidos.getListPedidos();
    }

  async presentAlert()
  {
    const alert = this.alertCtrl.create({
      subHeader: '¿Eliminar Pedido?',
      message: 'Si elimina el pedido no podra recuperarlo',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancelar');
          }
        },
        {
          text: 'Eliminar',
          handler: () => {
            console.log('Eliminar');
            this.deletePedido();
          }
        }
      ]
    });
    (await alert).present();
  }

  deletePedido(){
    this.listPedidos.deleteStorage();
    location.reload();
  }

  updatePedido(){
    this.router.navigate([`/add-pedido/${this.listPedidos.tienda}`]);
  }

  // Agregando valores al objeto del pedido
  savePedido(id: string){
    if (id) {
      this.listPedidos.getUserData(id)
        .subscribe(user => {
          this.user = user;
          console.log(user);
          this.addpedido.cliente.id = this.user[5].payload.node_.value_;
          this.addpedido.cliente.nombre = this.user[4].payload.node_.value_ + ' ' + this.user[0].payload.node_.value_ ;
          this.addpedido.cliente.telefono = this.user[5].payload.node_.value_;
          this.addpedido.cliente.direccion = this.authC.getUserLocation();
        });
    }
  }

  async confirmAlert()
  {
    const alert = await this.alertCtrl.create({
      header: 'SOLICITUD ENVIADA',
      message: 'El pedido ha sido enviado para que un repartidor lo acepte',
      buttons: ['ENTENDIDO']
    });
    await  alert.present();
  }

  async confirmPedido()
  {
    const alert = this.alertCtrl.create({
      subHeader: '¿Enviar Pedido?',
      message: 'Seguro que desea enviar el pedido',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancelar');
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            console.log('Aceptar');
            console.log('Pedido Confirmado');
            this.sendPedido();
            this.confirmAlert();
            localStorage.clear();
            this.ocultar();
            //this.router.navigate(['/tiendas']);
          }
        }
      ]
    });
    (await alert).present();
  }

  sendPedido(){
    this.listPedidos.addPedido(this.addpedido);
  }

  ocultar(){
    this.ocultarBtn = !this.ocultarBtn;
    this.estado = "Cancelar Solicitud";
  }

  changeEstado(){
    this.estado = "Confirmar Entrega";
  }


  async presentNotificacion() {
    const alert = this.alertCtrl.create({
      message: `<ion-avatar class="avatar"><img class="imagen-avatar" src="https://icons-for-free.com/iconfiles/png/512/business+costume+male+man+office+user+icon-1320196264882354682.png"></ion-avatar><p><center>Ricardo Arrobo</p><p>4/5</p><p>Quiere tomar tu pedido!</center></p>`,
      buttons:[
        {
          text: 'Aceptar',
          handler: () => {
            console.log('Aceptar');
            this.changeEstado();
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancelar');
          }
        }
      ]
    });
    (await alert).present();
  }


}