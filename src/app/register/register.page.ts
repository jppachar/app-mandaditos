import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {  AngularFireDatabase } from '@angular/fire/database';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {


  user: any = {};
  /*nombres: string = ""
  apellidos: string = ""
  telefono: number
  correo: string = ""
  contrasena: string = ""*/

  constructor(
    private afAuth: AngularFireAuth,
    private router: Router,
    private afDB: AngularFireDatabase
    ) { }

  ngOnInit() {
  }

  register(){
    if(this.user.correo && this.user.contrasena){
      this.afAuth.createUserWithEmailAndPassword(this.user.correo, this.user.contrasena).then((r) => {
        console.log(r);
        this.afDB.object('usuarios/' + r.user.uid).set({
          nombres: this.user.nombres,
          apellidos: this.user.apellidos,
          telefono: this.user.telefono,
          correo: this.user.correo,
          contrasena: this.user.contrasena,
          createdAt: Date.now(),
        }).then(()=>{
          this.router.navigateByUrl('/home');
        });
      }).catch(e => {
        console.log(e);
      })
    }
  }
}
