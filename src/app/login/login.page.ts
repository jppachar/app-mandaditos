import { Component, OnInit } from '@angular/core';
import { AuthService } from '../servicios/auth.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email: string;
  password: string;

  constructor(private alertCtrl: AlertController, private authService: AuthService, public router: Router) { }

  ngOnInit() {  }


  async presentAlert()
  {
    const alert = await this.alertCtrl.create({
      header: '  Usuario no encontrado',
      subHeader: 'No existe el usuario',
      message: 'Correo y/o contraseña incorrecta',
      buttons: ['OK']
    });
    await  alert.present();
  }

  onSubmitLogin()
  {
    this.authService.login(this.email, this.password).then(res => {
      this.router.navigate(['/tiendas']);
    }).catch(err => this.presentAlert());
  }

  onSubmitRegister()
  {
    this.router.navigate(['/register']);
  }

 
}
