import { AuthService } from './../servicios/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-add-pedido',
  templateUrl: './add-pedido.page.html',
  styleUrls: ['./add-pedido.page.scss'],
})
export class AddPedidoPage implements OnInit {

  nombreTienda: string;


  Productos = new Array();



  visibleAgregar: boolean = false;
  visibleUpdate: boolean =true;
  idProductoEditado:null;

  addProd={
    cantidad: " ",
    nombreProducto: " ",
    observacion: " "
  };

  constructor(public route: ActivatedRoute, public router: Router, private authC: AuthService, private alertCtrl: AlertController) { }

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap: ParamMap) =>{
      if(paramMap.has('nombreTienda')){
        this.nombreTienda = paramMap.get('nombreTienda');
      }
    });
    this.authC.setUserId();
  }


  editProducto(producto,id){

    console.log(producto);
    this.visibleAgregar=true;
    this.visibleUpdate=false;
    this.idProductoEditado=id;

    this.addProd={
      cantidad:producto.cantidad,
      nombreProducto: producto.nombreProducto,
      observacion: producto.observacion
    };

  }


  add(producto){
    this.Productos.push(producto);
    console.log(this.Productos);
    //this.localStorageProdList(producto);
    //this.getProdList();
    this.addProd={
      cantidad: " ",
      nombreProducto: " ",
      observacion: " "
    };
  }


  updateProducto(productoEditado){
    console.log("se actualizo en entrada");
    var i=this.Productos.indexOf(productoEditado.nombreProducto);
    

    this.Productos.splice(this.idProductoEditado,1);
    this.Productos.push(productoEditado);

    this.visibleAgregar=false;
    this.visibleUpdate=true;

    this.addProd={
      cantidad: " ",
      nombreProducto: " ",
      observacion: " "
    };

  }
  deleteProducto(position){
    
    var i=this.Productos.indexOf(position);
    this.Productos.splice(position,1);
    console.log("eliminar producto");
    
  }
/*
  getProdList(){
    var prodList = localStorage.getItem('localProdList');
    if(prodList == null){
      this.Productos = [];
    } else {
      this.Productos = JSON.parse(prodList);
    }
  }

  localStorageProdList(prodList) {
    localStorage.setItem('localProdList', JSON.stringify(prodList));
  }*/

  async presentAlert()
  {
    const alert = await this.alertCtrl.create({
      subHeader: 'Solicitud Incorrecta',
      message: 'No ha ingresado productos a la lista',
      buttons: ['ENTENDIDO']
    });
    await  alert.present();
  }

  confirmarPedido(){
    /* const totalPedido =  {
      cantidadTotal:
    }; */
    if(this.Productos.length !== 0){
      const cantidad:number = this.Productos.length;
      localStorage.setItem('datosPedido', JSON.stringify(this.Productos));
      localStorage.setItem('cantidadTotal', JSON.stringify(cantidad));
      localStorage.setItem('tienda', JSON.stringify(this.nombreTienda));
      this.router.navigate(['/pedidos']);
    }else{
      this.presentAlert();
    }
    
    //this.router.navigate([""]);
  }

  backMap(){
    localStorage.clear();
    this.router.navigate(['tiendas']);
  }
}
