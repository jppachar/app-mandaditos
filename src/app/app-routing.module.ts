import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes, CanActivate } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { NologinGuard } from './guards/nologin.guard';


const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then(m => m.RegisterPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule),
    canActivate: [NologinGuard]
  },
  {
    path: 'tiendas',
    loadChildren: () => import('./tiendas/tiendas.module').then(m => m.TiendasPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'add-pedido/:nombreTienda',
    loadChildren: () => import('./add-pedido/add-pedido.module').then( m => m.AddPedidoPageModule)
  },
  {
    path: 'pedidos',
    loadChildren: () => import('./pedidos/pedidos.module').then( m => m.PedidosPageModule)
  },
  {
    path: 'pedidos-solicitados',
    loadChildren: () => import('./repartidor/pedidos/pedidos.module').then( m => m.PedidosPageModule)
  },
  {
    path: 'detalle-pedido/:id',
    loadChildren: () => import('./repartidor/detalle-pedido/detalle-pedido.module').then( m => m.DetallePedidoPageModule)
  },



  // {
  //   path: 'tiendas',
  //   loadChildren: () => import('./tiendas/tiendas.module').then(m => m.TiendasPageModule)
  // },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

